<!doctype html>
    <head>
        @yield('title')

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="../../../public/css/index.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <style>
            body{
                margin: 0px;
                padding: 0px;
            }
            .header{
                color:black;
                background-color: rgb(45,178,171);
                height: 100px;
                width:100%;
                text-decoration-color: #005cbf;


            }
            .headerPicture{

                margin-top:10px;
                margin-left:5px;
                float: left;

            }
            h1{
                display: inline;
                font-size: 50px;
                font-family:'serif' ;
                text-decoration-color: #005cbf;

            }
            .headerText{
                display: inline;

                margin-left:450px;
                float:left ;
                margin-top: 25px;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;



            }
            .footer{

                color:white;
                background-color: rgb(45,178,171);
                height: 70px;
                width:100%;
            }





        </style>

    </head>

    <body>


        <header class="header">


               <img class="headerPicture" src=" {{ asset('images/logo.jpg') }}" height="80px" width="80px" alt=""/>

               <div class="headerText"><h1>Takmicarko</h1></div>


        </header>

        @yield('content')

        <div class="footer">

        </div>

    </body>
</html>
