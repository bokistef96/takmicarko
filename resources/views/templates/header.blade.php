@extends ('templates.layout')



@section('content')
    <style>
        .navigation {
            width: 100%;
            float: right;
            text-align: right;
            margin:15px;
        }

        .glavni{
            float: left;
            width: 100%;
            height: 768px;
            background-image: url({{asset('images/proba1.jpg')}});
        }
        .btn-primary{
            background-color: rgb(237,219,53);
            color: black;
        }
        .pocetno{
            margin-top: 170px;
            margin-left: 50px;
        }
        .tekst{
            width: 50%;
            margin: 50px;
            font-size: large;
        }
        .main{
            height: 800px;
        }
    </style>
    <div class="main">

    <div class="glavni">
        <div class="navigation">

            <button class="btn btn-primary" >Uloguj se | Registruj se</button>

        </div>
        <div class="tekst">
            <p class="font-weight-light">Takmičarko treba da cuva informacije o svakom registrovanom takmičaru kao i da generiše informacije o njemu. Zatim da vrši klasifikacije na osnovu oblasti takmičenja, gradu u kome se takmičenje održava kao i kom je razredu namenjeno. Sajt takodje omogucava rangiranje registrovanih takmičara i pamcenje njihovih rezultata na svim prethodnim takmičenjima na kojima su učestvovali.</p>
        </div>
        <div class="pocetno">
            <button class="btn btn-primary  onclick="{{ Redirect::to('/takmicenja') }}" ">Takmicenja</button>
            <button class="btn btn-primary">Takmicari</button>
        </div>
    </div>
    </div>
@endsection